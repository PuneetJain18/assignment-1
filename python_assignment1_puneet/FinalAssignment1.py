import SparkSql as from spark_sdk.sql
import json
import requests
import from pyspark.sql import DataFrame
import logging
import unittest

logger = logging.getLogger()

class testing(unittest.TestCase):

    def __init__(self):
        obj= TableFunctionalities()

    def test_overwrite(self):
        df1 = self.obj.overwrite(tableB='EmployeeProd', tableA='Employee');
        df2 = self.obj.overwrite(tableB='Employee', tableA='EmployeeProd');
        self.assertEqual(df1,df2,"Data written not equal")


class TableFunctionalities:

    def overwrite(self, tableA, tableB):
      #  Overwrite the destination table with data from source table
      try:
          queries =f'truncate table {tableB};' \
                   f'insert into {tableB} select * from {tableA}'
          df = SparkSql.execute_queries(queries=queries)
          return df
      except:
          logger.error("Error in overwriting")

    def append(self, tableA,tableB):
        #Append the destination table with data from source table
        try:
            df = SparkSql.execute_single_query(query=f'insert into {tableB} select * form {tableA};')
            return df
        except:
            logger.error("Error in appending")

    def delete_range(self,tableA , tableB, dateMin ,dateMax):
        #Delete the data in the destination table for a given date range
        #( start_date and end_date of the format YYYY-MM-DD) and then insert data for the same date range from A to B
        try:
            queries=f'delete from {tableB} where startDate >= {dateMin} and endDate <={dateMax}; ' \
                    f'insert into {tableB} select * from {tableA} where startDate >= {dateMin} and endDate <={dateMax};'

            df = SparkSql.execute_queries(queries=queries)
            return df
        except:
            logger.error("Error in delete Range")

    def delete_basis_query(self,deleteQuery,updateQuery,tableA,tableB):
        # Delete the data in the destination table on the basis of a query and then append the data from A to B
        # (append query will also be given)
        try:
            queries = f'delete from {tableB} where {deleteQuery}; ' \
                      f'insert into {tableB} select * from {tableA} where {updateQuery};'
            df = SparkSql.execute_queries(queries=queries)
            return df
        except:
            logger.error("Error in delete on basis of query")


if __name__== "__main__":
    tb = TableFunctionalities()
    tb.append(tableA = "EMPLOYEE", tableB="Employee_prod")
    tb.overwrite(tableA = "EMPLOYEE", tableB="Employee_prod")
    tb.delete_basis_query(deleteQuery="salary >20000",updateQuery="salary < 100000",tableB="Employee_prod", tableA="Employee")
    tb.delete_range(tableB='Employee', tableA='Employee_prod',dateMin='2018-08-07', dateMax='2020-09-09')


